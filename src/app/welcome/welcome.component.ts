import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { WelcomeDataService } from '../service/data/welcome-data.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  // message = 'Some welcome message';
  welcomeMessageFromService: string
  name = ''
  serverError = false
  serverErrorMessage = "We apologise. The server is under maintenance"

  constructor(private route: ActivatedRoute,
    private service: WelcomeDataService

  ) { }

  ngOnInit() {
    // console.log(this.message);
    this.name = this.route.snapshot.params['name']
  }

  getWelcomeMessage() {
    // console.log(this.service.executeHelloWorldbeanService())
    this.service.executeHelloWorldbeanService().subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
      // response => console.log(response.message)
    );
 
    console.log("last line")
  }

  getWelcomeMessagePathVariable() {
    // console.log(this.service.executeHelloWorldbeanService())
    this.service.executeHelloWorlPathVariableService(this.name).subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
      // response => console.log(response.message)
    );

    console.log("last line")
  }

  handleSuccessfulResponse(response) {
    // console.log(response);
    // console.log(response.message)
    this.serverError = false
    this.welcomeMessageFromService = response.message

  }

  handleErrorResponse(error){
    this.serverError = true
  //  console.log(this.welcomeMessageFromService = error.message + " Errorrrr")

  }

}
