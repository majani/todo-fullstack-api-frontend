import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Router } from '@angular/router';

export class Todo {
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date,
  ) { }
}

@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})
export class ListTodosComponent implements OnInit {

  todos: Todo[];
  message: string;
  todo : Todo;
  //[
  // new Todo(1, 'Learn to dance', false, new Date()),
  // new Todo(2, 'Become an expert in Angular', false, new Date()),
  // new Todo(3, 'Visit India', false, new Date()),
  // new Todo(4, 'Get Employed', false, new Date()),
  // new Todo(5, 'Visit Arusha', false, new Date())
  // {id : 1, description: 'Learn to dance'},
  // {id : 2, description: 'Become an expert in Angular'},
  // {id : 3, description: 'Visit India'}
  //]

  // todo = {
  //   id : 1,
  //   description : 'Learn to dance'

  // }

  constructor(private todoService: TodoDataService,
    private router: Router) { }

  ngOnInit() {

    this.refreshTodos();

  }

  refreshTodos() {
    this.todoService.retrieveAllTodos('in28minutes').subscribe(
      response => {
        console.log(response);
        this.todos = response;  //We save the response into todos a Todo []

      }
    )
  }

  deleteTodo(id) {
    console.log(`delete to do ${id}`);
    this.todoService.deleteTodo('in28minutes', id).subscribe(
      response => {
        console.log(response)
        this.message = (`Delete of Todo ${id} is successful`)
        this.refreshTodos()
 
      }
    )
  }

  updateTodo(id) {
    console.log(`Update Todo ${id}`);
    this.router.navigate(['todos',id]);
  }


  addTodo(){
    this.router.navigate(['todos',-1]);
    

  }

}
