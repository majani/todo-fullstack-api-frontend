import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


export class HelloWorldBean {
  constructor(public message: string) { }
}
@Injectable({
  providedIn: 'root'
})

export class WelcomeDataService {

  constructor(
    private http: HttpClient
  ) { }

  executeHelloWorldbeanService() {
    return this.http.get<HelloWorldBean>("http://localhost:8080/hello-world-bean")
  }

  executeHelloWorlPathVariableService(name) {
    // let basicAuthHeaderString = this.createBasicAuthenticationHttpHeader();
    // let headers = new HttpHeaders({
    //   Authorization: basicAuthHeaderString               //create an instance of HttpHeaders and poppulate it with the headers 

    // })
    return this.http.get<string>(`http://localhost:8080/welcome/pathVariable/${name}`)
  }


  // createBasicAuthenticationHttpHeader() {
  //   let username = 'in28minutes';
  //   let password = 'dummy';
  //   let basicAuthHeaderString = 'Basic ' + window.btoa(username + ':' + password); //byte64 representation of a combination of username and password appended by Basic
  //   return basicAuthHeaderString;
  // }

  // Access to XMLHttpRequest at 'http://localhost:8080/hello-world-bean/in28minutes' from origin 'http://localhost:4200' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. 
}


