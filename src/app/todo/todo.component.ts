import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Todo } from '../list-todos/list-todos.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  id: number;
  todo: Todo;

  constructor(private todoService: TodoDataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.todo = new Todo(this.id, '', false, new Date());// a default to do since we are getting Todo from the service asynchronously and there might be a delay

    if (this.id != -1) {//if id is not -1 then retrieve Todo..otherwise we don't need to retrieve we create a new Todo
      this.todoService.retrieveTodo("in28minutes", this.id).subscribe(
        data => this.todo = data
      )
    }
  }

  saveTodo() {
    //create
    if (this.id == -1) {
      this.todoService.createTodo('in28minutes', this.todo).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['todos'])
        }
      )
    

    }
    else{
      //update
    this.todoService.updateTodo('in28minutes', this.id, this.todo).subscribe(
      data => {
        console.log(data)
        this.router.navigate(['todos'])
      }
    )
    }

  }


 
   

}
